angular.module('openWeatherApp.services', ['ngResource'])

.factory('openWeatherMap', function bar($resource) {
  const apiKey = 'cbd6948bdf3b766d6e4cb1d802b12b78';
  const apiBaseUrl = 'http://api.openweathermap.org/data/2.5/';
  return $resource(apiBaseUrl + ':path/:subPath?q=:location',
    {
      APPID: apiKey,
      mode: 'json',
      callback: 'JSON_CALLBACK',
      units: 'metric',
      lang: 'en',
    },
    {
      queryForecastDaily: {
        method: 'JSONP',
        params: {
          path: 'forecast',
          subPath: 'daily',
          cnt: 1,
        },
        isArray: false,
        headers: {
          'x-api-key': apiKey,
        },
      },
    }
);
});

