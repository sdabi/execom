angular.module('openWeatherApp.controllers', ['ngTagsInput'])
.controller('openWeatherCtrl', ['$scope', 'openWeatherMap', function ($scope, openWeatherMap) {
  $scope.showData = false;
  $scope.Math = Math.floor;
  $scope.date = new Date();
  $scope.location = [];
  $scope.onChange = function () {
    $scope.niz = [];
    angular.forEach($scope.location, function (obj) {
      $scope.getForecastByLocation(obj.text);
    });
  };
  $scope.getForecastByLocation = function (item) {
    $scope.showData = true;
    $scope.forecast = openWeatherMap.queryForecastDaily({
      location: item,
    });
    $scope.niz.push($scope.forecast);
  };

  $scope.setLocation = function (loc) {
    $scope.location = loc;
    $scope.getForecastByLocation();
  };    
}]);